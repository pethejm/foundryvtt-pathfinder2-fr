Tous les contenus en anglais présents dans les fichiers des sous-répertoires proviennent
d'une extraction de bases de données du système pathfinder-2e: 
https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/tree/master/dist/packs

Ces textes sont assujettis aux conditions de l'[OPEN GAME LICENSE](../OPEN%20GAME%20LICENSE) et de la PCUP de Paizo.

Les traductions officielles sont celles issues de la version française officielle du jeu dont la licence appartient à la société Black Book Editions : https://www.black-book-editions.fr/ Les textes ont été parfois remaniés pour supprimer les numéros de page et les numéros des tables et adaptés au format wiki pour créer des liens.

Les traductions libres données sont les traductions de **rectulo** et de membres du site https://pathfinder-fr.org. En cas d'utilisation autorisée, vous devez créditer le site.
